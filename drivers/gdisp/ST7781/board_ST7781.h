/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 * http://ugfx.org/license.html
 *
 * Example implementation used with ST7781 LCD controller, on STM32F103 using
 * ChibiOS.
 *
 * LCD has 8-bit wide interface, connected to PORTA[0..7].
 *
 * CS - PB0
 * RS - PB1
 * WR - PB10
 * CLR- PB11
 * RST - PB15
 *
 *
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

// For a multiple display configuration we would put all this in a structure and then
//	set g->board to that structure.
#define SET_CS		palSetPad(GPIOB, 0);
#define CLR_CS		palClearPad(GPIOB, 0);
#define SET_RS		palSetPad(GPIOB, 1);
#define CLR_RS		palClearPad(GPIOB, 1);
#define SET_WR		palSetPad(GPIOB, 10);
#define CLR_WR		palClearPad(GPIOB, 10);
#define SET_RD		palSetPad(GPIOB, 11);
#define CLR_RD		palClearPad(GPIOB, 11);

#define SET_RST     palSetPad(GPIOC, 15);
#define CLR_RST     palClearPad(GPIOC, 15);

#define PORTA_MASK ((ioportmask_t)0xFF)

static inline void init_board(GDisplay *g)
{

	// As we are not using multiple displays we set g->board to NULL as we don't use it.
	g->board = 0;

	switch(g->controllerdisplay) {
	case 0:											// Set up for Display 0
		palSetGroupMode(GPIOA, PORTA_MASK, 0, PAL_MODE_OUTPUT_PUSHPULL);
		palSetPadMode(GPIOB, 0, PAL_MODE_OUTPUT_PUSHPULL);
		palSetPadMode(GPIOB, 1, PAL_MODE_OUTPUT_PUSHPULL);
		palSetPadMode(GPIOB, 10, PAL_MODE_OUTPUT_PUSHPULL);
		palSetPadMode(GPIOB, 11, PAL_MODE_OUTPUT_PUSHPULL);
		palSetPadMode(GPIOC, 15, PAL_MODE_OUTPUT_PUSHPULL);
		// Configure the pins to a well know state
		SET_RS;
		SET_RD;
		SET_WR;
		CLR_CS;
        SET_RST;
		break;
	}
}

static inline void post_init_board(GDisplay *g)
{
	(void) g;
}

static inline void setpin_reset(GDisplay *g, bool_t state)
{
	(void) g;

    if (state) {
        CLR_RST;
    }  else
        SET_RST; 
	/* Nothing to do here - reset pin tied to Vcc */
}

static inline void set_backlight(GDisplay *g, uint8_t percent)
{
	(void) g;
	(void) percent;
    
	/* Nothing to do here - Backlight always on */
}

static inline void acquire_bus(GDisplay *g)
{
	(void) g;
}

static inline void release_bus(GDisplay *g)
{
	(void) g;
}

static inline void write_index(GDisplay *g, uint16_t index)
{
	(void) g;

    CLR_RS;
    SET_RD;
    SET_WR;
    
	palWritePort(GPIOA, index >> 8);
    CLR_WR;
    SET_WR;
	palWritePort(GPIOA, index);
    CLR_WR;
    SET_WR;
    SET_RS;    
}

static inline void write_data(GDisplay *g, uint16_t data)
{
	(void) g;

    SET_RS;
    SET_RD;
    SET_WR;
	palWritePort(GPIOA, data>>8);
	CLR_WR;
	SET_WR;
	palWritePort(GPIOA, data);
    CLR_WR;
    SET_WR;
}

static inline void setreadmode(GDisplay *g)
{
	(void) g;

	// change pin mode to digital input
	palSetGroupMode(GPIOA, PORTA_MASK, 0, PAL_MODE_INPUT);
	CLR_RD;
}

static inline void setwritemode(GDisplay *g)
{
	(void) g;
	
	// change pin mode back to digital output
	SET_RD;
	palSetGroupMode(GPIOA, PORTA_MASK, 0, PAL_MODE_OUTPUT_PUSHPULL);
}

static inline uint16_t read_data(GDisplay *g) {
	(void) g;

	return palReadPort(GPIOA) & PORTA_MASK;
}

#if defined(GDISP_USE_DMA)
	#error "GDISP - ST7781: The GPIO interface does not support DMA"
#endif

#endif /* _GDISP_LLD_BOARD_H */

